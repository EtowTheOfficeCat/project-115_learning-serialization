﻿using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private string subPath = "/PlayerData.txt";
    [SerializeField]private PlayerDefaultSO defaultData;
    [SerializeField] private Transform weaponSpawnPoint;
    private string playerName;
    private int score;
    private int highScore;
    private int level;
    private Vector3 spawnPos;
    private int currentHealth;
    private Weapon equippedWeapon;
    private char delimiter = '/';


    [SerializeField] private bool useSavedData = false;
    private List<String[]> lines = new List<string[]>();
   
    void Start()
    {
        if (!useSavedData)
        {
            SetUpPlayer();
        }
        else
        {
            LoadPlayerFromFile();
        }
       
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            SavePlayerDataToFile();
        }
    }
    void SetUpPlayer()
    {
        score = defaultData.score;
        level = defaultData.level;
        spawnPos = defaultData.spawnPos;
        currentHealth = defaultData.currentHealth;
        playerName = defaultData.playerName;
        equippedWeapon = defaultData.equippedWeapon;
        transform.position = spawnPos;
        equippedWeapon = Instantiate<Weapon>(defaultData.equippedWeapon, weaponSpawnPoint.position, Quaternion.identity);
        equippedWeapon.transform.parent = weaponSpawnPoint;
    }

    

    void LoadPlayerFromFile()
    {
        using (StreamReader reader = new StreamReader(Application.streamingAssetsPath + subPath))
        {

            string[] tokens;

            playerName = reader.ReadLine().Split(delimiter)[1];
            score = int.Parse(reader.ReadLine().Split(delimiter)[1]);
            level = int.Parse(reader.ReadLine().Split(delimiter)[1]);

            tokens = reader.ReadLine().Split(delimiter);
            spawnPos = new Vector3(float.Parse(tokens[1]), float.Parse(tokens[2]), float.Parse(tokens[3]));
            
            currentHealth = int.Parse(reader.ReadLine().Split(delimiter)[1]);
            equippedWeapon = Instantiate<Weapon>(defaultData.equippedWeapon, weaponSpawnPoint.position, Quaternion.identity);
            equippedWeapon.transform.parent = weaponSpawnPoint;

            equippedWeapon.name = reader.ReadLine().Split(delimiter)[1]; 
            equippedWeapon.durability = int.Parse(reader.ReadLine().Split(delimiter)[1]); 
            equippedWeapon.curWHealth = int.Parse(reader.ReadLine().Split(delimiter)[1]); 
            equippedWeapon.attack = int.Parse(reader.ReadLine().Split(delimiter)[1]);

            tokens = reader.ReadLine().Split(delimiter);
            equippedWeapon.color = new Color(float.Parse(tokens[1]), float.Parse(tokens[2]), float.Parse(tokens[3]));

            transform.position = spawnPos;
        }
    }
    void SavePlayerDataToFile()
    {

        string[] lines = new string[10];
        lines[0] = "sName"+ delimiter + playerName;
        lines[1] = "iScore"+ delimiter + score.ToString();
        lines[2] = "iLevel"+ delimiter + level.ToString();
        string x = spawnPos.x.ToString();
        string y = spawnPos.y.ToString();
        string z = spawnPos.z.ToString();
        lines[3] = $"vSpawnPos{delimiter}{x}{delimiter}{y}{delimiter}{z}";
        lines[4] = "iHealth" + delimiter + currentHealth.ToString();
        lines[5] = "sWeapon" + delimiter + equippedWeapon.name;
        lines[6] = "iDurability" + delimiter + equippedWeapon.durability;
        lines[7] = "iCurrenHealth" + delimiter + equippedWeapon.curWHealth;
        lines[8] = "iAttack" + delimiter + equippedWeapon.attack;
        string r = equippedWeapon.color.r.ToString();
        string b = equippedWeapon.color.b.ToString();
        string g = equippedWeapon.color.g.ToString();
        lines[9] = $"sColor{delimiter}{r}{delimiter}{b}{delimiter}{g}";

        using (StreamWriter sw = new StreamWriter(Application.streamingAssetsPath + subPath))
        {
            foreach (string line in lines)
            {
                sw.WriteLine(line);
            }
        }
    }
    // string GetFloatString(string input)
    //{
    //    var splitInput = input.Split('.', 'f');
    //    return splitInput[0] + "," + splitInput[1];
    //}
}

