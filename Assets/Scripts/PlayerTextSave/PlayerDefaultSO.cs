﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerDefaultData", menuName = "Data/PlayerDefaultData")]
public class PlayerDefaultSO : ScriptableObject
{
    public int score;
    public int highScore;
    public int level;
    public Vector3 spawnPos;
    public int currentHealth;
    public string playerName;
    public Weapon equippedWeapon;
}
