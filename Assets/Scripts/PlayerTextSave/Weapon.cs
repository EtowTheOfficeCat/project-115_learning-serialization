﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public string weaponName;
    public int attack;
    public int durability;
    public int curWHealth;
    public Color color;

    private void Start()
    {
        transform.GetChild(0).GetComponent<Renderer>().material.color = color;
    }
}
