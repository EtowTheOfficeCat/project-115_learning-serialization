﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private Player playerPrefab;

    void Start()
    {
        Instantiate<Player>(playerPrefab);
    }

}
