﻿using System.Xml.Serialization;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class XmlManger : Singleton<XmlManger>
{
    private string pathToFile;
    public Database database;

    private void Awake()
    {
        pathToFile = Application.streamingAssetsPath + "/XML/ColoredCube_data.xml"; 
    }


    public List<ColoredCubeData> LoadColoredCubes()
    {
        if (File.Exists(pathToFile))
        {
            using(StreamReader stream = new StreamReader(pathToFile))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Database));
                database = serializer.Deserialize(stream) as Database;
                return database.coloredCubeDataList;
            }
        }
        else
        {
            return null;
        }
    }

    public void SaveColoredCubes(List<ColoredCubeData> dataList)
    {
        database.coloredCubeDataList = dataList;
        Encoding encoding = Encoding.GetEncoding("UTF-8");
        using (StreamWriter stream = new StreamWriter(pathToFile, false, encoding))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Database));
            serializer.Serialize(stream, database);
        }
    }

    [System.Serializable]
    public class Database
    {
        [XmlArray("ColoredCubes")]
        public List<ColoredCubeData> coloredCubeDataList = new List<ColoredCubeData>();
    }
}
