﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredCube : MonoBehaviour
{
    public ColoredCubeData Data
    {
        get { return data; }
        set { data = value; }
    }
    [SerializeField]
    private ColoredCubeData data;

    public void Init()
    {
        GetComponent<Renderer>().material.color = data.Color;
        transform.localPosition = data.Position;
        transform.localScale = data.Scale;
        gameObject.name = data.Name;
    }
}
