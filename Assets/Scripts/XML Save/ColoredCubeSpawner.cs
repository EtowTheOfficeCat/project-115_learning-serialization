﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredCubeSpawner : MonoBehaviour
{
    public int numberOfRandomeCubes = 5;
    public void Spawn(SpawnMode spawnMode)
    {
       
        SpawnCubes(Prepare(spawnMode));
    }
    public List<ColoredCubeData> Prepare(SpawnMode spawnMode)
    {
        List<ColoredCubeData> dataList;
        if(spawnMode == SpawnMode.Random)
        {
            dataList = GetRandomData(numberOfRandomeCubes);
        }
        else
        {
            dataList = XmlManger.Instance.LoadColoredCubes();
            
        }
        return dataList;

    }

    public void SpawnCubes(List<ColoredCubeData> dataList)
    {
        for (int i = 0; i < dataList.Count; i++)
        {
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            var colCube = cube.AddComponent<ColoredCube>();
            colCube.Data = dataList[i];
            colCube.Init();
            colCube.transform.parent = transform;
                
        }
    }

    private List<ColoredCubeData> GetRandomData(int count)
    {
        var dataList = new List<ColoredCubeData>(count);
        for (int i = 0; i < count; i++)
        {
            dataList.Add(new ColoredCubeData(GetRandomColor(), GetRandomVector3(3f, false), GetRandomVector3(1.5f, true), GetRandomString(5)));
        }
        return dataList;
    }
    private Color GetRandomColor()
    {
        return Random.ColorHSV(0f, 1f, 0.4f, 0.8f, 0.7f, 0.8f);
    }
    
    private Vector3 GetRandomVector3(float range, bool onlypositive)
    {
        if (onlypositive)
        {
            return ((Random.insideUnitSphere *  0.5f ) + Vector3.one) * range;
        }
        else { return Random.insideUnitSphere * range; }

        
    }

    private string GetRandomString(int length)
    {
        string pool = "QWERTZUIOPASDFGHJKLYXCVBNM";
        StringBuilder sb = new StringBuilder();
        int seed = Random.Range(0, 500);
        System.Random random = new System.Random(seed);

        for (int i = 0; i < length; i++)
        {
            var c = pool[random.Next(0, pool.Length)];
            sb.Append(c);
        }
        return sb.ToString();
    }
}
