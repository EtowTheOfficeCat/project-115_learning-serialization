﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredCubemanager : MonoBehaviour
{
    public SpawnMode spawnMode = SpawnMode.Random;

    public void setSpawnMode (int index)
    {
        spawnMode = (SpawnMode)index;
    }

    public void SpawnColoredCubes()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        var spawner = GetComponent<ColoredCubeSpawner>();
        spawner.Spawn(spawnMode);
    }

    public void SaveColoredCubes()
    {

        List<ColoredCubeData> dataList = new List<ColoredCubeData>();
        foreach (Transform child in transform)
        {
            dataList.Add(child.GetComponent<ColoredCube>().Data);
            XmlManger.Instance.SaveColoredCubes(dataList);
        }
    }
}

public enum SpawnMode
{
    Random, XML
}
