﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BinarySerializer : MonoBehaviour
{
    public void SaveGameData(string path, string filename, GameSaveData data)
    {
       
        if (File.Exists(path + filename))
        {
            Debug.Log("overwriting");
            
        }
        using (FileStream stream = File.Create(path + filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(stream, data);
        }
    }

    public GameSaveData LoadGameData(string path, string filename)
    {
        if (!File.Exists(path + filename))
        {
            Debug.LogError("can't load file that does not exist");
            return null;
        }
        else
        {
            using (FileStream stream = new FileStream(path + filename, FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                return (GameSaveData) bf.Deserialize(stream);
            }
        }
        
    }
}

[System.Serializable]
public class GameSaveData
{
    public int CurrentLevel => currentLevel;
    private int currentLevel;
    public string playerName;
    public SerializableVector3 playerPosition;

    public GameSaveData(int currentLevel, string playerName, Vector3 playerPosition)
    {
        this.currentLevel = currentLevel;
        this.playerName = playerName;
        this.playerPosition = new SerializableVector3(playerPosition);
    }
}
[System.Serializable]
public struct SerializableVector3
{
    public float x;
    public float y;
    public float z;

    public SerializableVector3(Vector3 v)
    {
        x = v.x;
        y = v.y;
        z = v.z;

    }

    public Vector3 toVector3()
    {
        return new Vector3(x, y, z);
    }
}
