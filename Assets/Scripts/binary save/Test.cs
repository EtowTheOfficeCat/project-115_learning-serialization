﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    private BinarySerializer serializer;
    [SerializeField] private GameSaveData gamedata;
    [SerializeField] private string gameDataPath;
    [SerializeField] private string dataFileName;

    void Awake()
    {
        serializer = GetComponent<BinarySerializer>();
        gamedata = new GameSaveData(3, "Saruman" , Vector3.one);


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            serializer.SaveGameData(Application.dataPath + "/" + gameDataPath + "/", dataFileName, gamedata);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            gamedata = serializer.LoadGameData(Application.dataPath + "/" + gameDataPath + "/", dataFileName);
            Vector3 pos = gamedata.playerPosition.toVector3();
            transform.position = pos;
            
        }
    }
}
