﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data" , menuName = "Data/TestData")]
public class TestDataSO : ScriptableObject
{
    public string Name = "Agatha";
    public float Height = 1.80f;
    public int Age = 279;
    public Vector3 StartPosition = Vector3.zero;
}
